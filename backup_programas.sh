#!/bin/bash
echo "                                                           "
echo "Backup dos BANCOS iniciado em $(date +%d"/"%b"/"%Y" as "%T)" >> /media/log/bkp_SIA_log.txt
### MONTAR OS HDS
mount -t nfs 10.14.180.4:/media/arquivos /media/fonte
##### BACKUP SIA 
cp -v -R -f /media/fonte/programas/siabkp /media/backup1/BANCOS && echo "Backup do BD SIA realizado com sucesso" >> /media/log/bkp_SIA_log.txt
##### BACKUP SISCAR
cp -v -R -f /media/fonte/programas/siscarbkp /media/backup1/BANCOS&& echo "Backup do BD SISCAR realizado com sucesso" >> /media/log/bkp_SIA_log.txt
##### 
echo -e "Backup completo concluidos em $(date +%d"/"%b"/"%Y" as "%T)\n\n" >> /media/log/bkp_SIA_log.txt
umount -l //10.14.180.4/arquivos
