#!/bin/bash
#set: fileformat=unix
pesquisa_ip(){
data=$(date +%d_%b_%Y-%T)
echo "Digite o PRE-FIXO onde deseja pesquisar"
echo -n "--> "
read pre
echo 'Digite a faixa de IPs a ser pequisados: '
echo -n 'Inico: '
read ini
echo -n 'Fim: '
read fim
echo -n 'De um nome ao aquivo onde as informações serão salvas: '
read arquivo
clear
echo "Script em execução, esta opção pode demorar um pouco..."
	for ((ini=ini;ini<fim+1;ini++))
		do
			echo 'Disparando contra 10.14.'$pre'.'$ini
			nmblookup -A 10.14.$pre.$ini >> /media/arquivos/programas/$arquivo"-"$data.txt
		done
clear
echo "Script executado com sucesso procure o resultado em /media/arquivos/"$arquivo"-"$data.txt  
}

montar(){
	interno(){
		echo "Qual DEV deseja montar?"
		read hd_escolhido
		echo "Onde deseja montar este HD (verifique se a pasta realmente existe) ?"
		echo -n "--> "
		read caminho
		if [[ -d $caminho ]]; then
			mount /dev/$hd_escolhido $caminho
		else
		echo "O Diretorio " $caminho " nao existe, por favor tente novamente." 
		fi
	}
	externo(){
		echo "Qual DEV deseja montar?"
		read hd_escolhido
		echo "Onde deseja montar este HD (verifique se a pasta realmente existe) ?"
		echo -n "--> "
		read caminho
		if [[ -d $caminho ]]; then
			mount /dev/$hd_escolhido $caminho
		else
		echo "O Diretorio " $caminho " nao existe, por favor tente novamente." 
		fi
	}

echo "Montar HD em rede ou INTERNO(HDs ligados fisicamente a maquina)?"
echo "1 - INTERNO"
echo "2 - REDE"
echo -n "--> "
read op

case op in
	1)	
		interno
		;;
	2)
		externo
		;;
esac
		
}