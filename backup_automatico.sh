#!/bin/bash
arquivos="/media/backup"
backup="/media/backup"
backup_local="/media/bkp1/"

function_backup(){
date >> /media/arquivos/programas/log_backup.txt
echo "ESPERE MONTANDO DIRETORIOS ENVOLVIDOS"
##montar o HD de backup no linux
mount -t cifs //10.14.180.4/arquivos /media/backup/ -o username=root,password=141202
chmod -R 777 /media/backup/*
#mount -t cifs $cronos $backup -o username=fernando,password=141202
sleep 3
echo "ORGANIZANDO AS COISAS...."
##remove todo backup anterior para substituir pelo novo 
#rm -f -r $backup/*
sleep 3
echo "PRONTO PARA O INICIO...."
sleep 10
# copia dos arquivos para o backup 
cp -f -R -v -u $arquivos/cimic $backup_local
cp -f -R -v -u $arquivos/cpd $backup_local
cp -f -R -v -u $arquivos/cras $backup_local
cp -f -R -v -u $arquivos/financas $backup_local
cp -f -R -v -u $arquivos/infra $backup_local
cp -f -R -v -u $arquivos/judiciaria $backup_local
cp -f -R -v -u $arquivos/np $backup_local
cp -f -R -v -u $arquivos/producao $backup_local
cp -f -R -v -u $arquivos/saude $backup_local
cp -f -R -v -u $arquivos/seguranca $backup_local
cp -f -R -v -u $arquivos/turno1 $backup_local
cp -f -R -v -u $arquivos/turno2 $backup_local
cp -f -R -v -u $arquivos/turno3 $backup_local
cp -f -R -v -u $arquivos/turno4 $backup_local
cp -f -R -v -u $arquivos/sindicancia $backup_local
cp -f -R -v -u $arquivos/publico $backup_local
cp -f -R -v -u $arquivos/rol $backup_local
cp -f -R -v $arquivos/adm $backup_local 
cp -f -R -v $arquivos/notes $backup_local
cp -f -R -v $arquivos/peculio $backup_local
chmod -R 777 $arquivos/*
chmod -R 777 $backup_local/*
sleep 3
#desmonta o HD do servidor
umount -l /dev/sdc1
echo "BACKUP DE "$arquivos" COMPLETO, POR FAVOR VERIFIQUE SUA INTEGRIDADE EM "$backup_local
date >> /media/arquivos/programas/log_backup.txt
}
function_backup

