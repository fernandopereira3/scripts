#!/bin/bash
arquivos="/media/arquivos"
backup="/media/backup"

#  /*.mpeg /*.mp3/*.wma/*.wmp/*.mpg/*.exe /*.mkv /*.flv /*.avi /*.mov /*.rmvb /*.vob /*.ac3 /*.dts /*.dat /*.srt /*.mp4 /*.wmv

function apaga_lixeira(){

echo "Clear da lixeira em progresso"
sleep 5
clear
echo "Atencao essa opcao e irreversivel"
clear
find $arquivos -empty -delete
rm -R -f -v $arquivos/lixeira/*
find $arquivos/cimic -name "*.mp3" -delete
sleep 5
find $arquivos/cpd -name "*.mp3" -delete
sleep 5
find $arquivos/financas -name "*.mp3" -delete
sleep 5
find $arquivos/infra -name "*.mp3" -delete
sleep 5
find $arquivos/judiciaria -name "*.mp3" -delete
sleep 5
find $arquivos/np -name "*.mp3" -delete
sleep 5
find $arquivos/producao -name "*.mp3" -delete
sleep 5
find $arquivos/seguranca -name "*.mp3" -delete
sleep 5
find $arquivos/turno 1 -name "*.mp3" -delete
sleep 5
find $arquivos/turno 2 -name "*.mp3" -delete
sleep 5
find $arquivos/turno 3 -name "*.mp3" -delete
sleep 5
find $arquivos/turno 4 -name "*.mp3" -delete
sleep 5
find $arquivos/publico -name "*.mp3" -delete
sleep 5
find $arquivos/rol -name "*.mp3" -delete
sleep 5
find $arquivos -name "~*.*" -delete
find $arquivos -name "*.db" -delete
sleep 10
clear
echo "Feito, espero que esteja tudo certo :| "
}
apaga_lixeira
