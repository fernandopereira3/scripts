#!/bin/bash

data=$(date +"%d-%b-%Y")

mount -t nfs 10.14.180.4:/media/arquivos/programas /media/bkp
cd /media/bkp/siscarbkp/
mysqldump -uroot -pfuturo07 siscar > siscar_$data.sql
cd /
umount -l 10.14.180.4:/media/arquivos/programas
