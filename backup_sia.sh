#!/bin/bash

data=$(date +"%d-%b-%Y")
diretorio =$(mkdir /media/backups/siabkp/sia-$data)

mount -t nfs 10.14.180.4:/media/arquivos/programas /media/backups/

cp -R -f -v /home/sia/banco/* /media/backups/siabkp
echo "backup do dia"$data >> /media/backup/siabkup.txt

umount -l 10.14.180.4:/media/arquivos/programas