#!/bin/bash

# Digite a versão desejada
bacula_version="9.4.3"

# Digite a chave recebida por email
bacula_key="XXXXXXXXXXXXX"


# Requisitos para instalar o Bacula por pacotes
dnf install -y zip wget bzip2


# Download da chaves do repositório
wget -c https://www.bacula.org/downloads/Bacula-4096-Distribution-Verification-key.asc -O /tmp/Bacula-4096-Distribution-Verification-key.asc


# Adicionar chave no repositório local
rpm --import /tmp/Bacula-4096-Distribution-Verification-key.asc


# Criar o repositório do Bacula Community
echo "[Bacula-Community]
name=CentOS - Bacula - Community
baseurl=http://www.bacula.org/packages/${bacula_key}/rpms/${bacula_version}/el7/x86_64/
enabled=1
protect=0
gpgcheck=0" > /etc/dnf.repos.d/bacula-community.repo


###################################################################
# Instalar o Banco de Dados MySQL ou PostgreSQL
# Selecione os comandos de acordo com a opção desejada


#==================================================================
# Instalar o MySQL
rpm --import /tmp/RPM-GPG-KEY-mysql
wget -c http://dev.mysql.com/get/mysql57-community-release-el7-9.noarch.rpm -O /tmp/mysql57-community-release-el7-9.noarch.rpm
rpm -ivh /tmp/mysql57-community-release-el7-9.noarch.rpm
dnf install -y mysql-community-server
mysqld --initialize-insecure --user=mysql
systemctl enable mysqld
systemctl start mysqld
dnf install -y bacula-mysql


# Criar o banco de dados do Bacula com MySQL
/opt/bacula/scripts/create_mysql_database
/opt/bacula/scripts/make_mysql_tables
/opt/bacula/scripts/grant_mysql_privileges


#==================================================================
# Instalar PostgreSQL
dnf install -y postgresql-server
dnf install -y bacula-postgresql --exclude=bacula-mysql
postgresql-setup initdb


# Habilitar e iniciar o PostgreSQL durante o boot
systemctl enable postgresql
systemctl start postgresql


# Criar o banco de dados do Bacula com PostgreSQL
su - postgres -c "/opt/bacula/scripts/create_postgresql_database"
su - postgres -c "/opt/bacula/scripts/make_postgresql_tables"
su - postgres -c "/opt/bacula/scripts/grant_postgresql_privileges"
###################################################################

# Desabilita selinux:
setenforce 0
sudo sed -i "s/enforcing/disabled/g" /etc/selinux/config
# Regras de Firewall
firewall-cmd --permanent --zone=public --add-port=9101-9103/tcp
firewall-cmd --reload

# Habilitar o início dos daemons durante o boot
systemctl enable bacula-fd.service
systemctl enable bacula-sd.service
systemctl enable bacula-dir.service


# Iniciar os daemons do Bacula
systemctl start bacula-fd.service
systemctl start bacula-sd.service
systemctl start bacula-dir.service


# Criar atalho em /usr/sbin com os binários do Bacula
# Isso permite rodar os daemons e utilitários
# Sem entrar no diretório /opt/bacula/bin
for i in `ls /opt/bacula/bin`; do
    ln -s /opt/bacula/bin/$i /usr/sbin/$i;
done


# Substituir o endereço do bconsole.conf para localhost por padrão
sed '/[Aa]ddress/s/=\s.*/= localhost/g' -i  /opt/bacula/etc/bconsole.conf