#!/bin/bash

data=$(date +%d_%b_%Y-%T)

mount /dev/sda1 /media/backup_local/
mkdir /media/backup_local/sia/siabkp_$data/
chmod -R 777 /media/backup_local/sia/*
sleep 5
cp -f -v /media/arquivos/programas/siabkp/dbsia.fdb /media/backup_local/sia/siabkp_$data/
cp -f -v /media/arquivos/programas/siabkp/dbsia_documentos.fdb /media/backup_local/sia/siabkp_$data/
umount -l /dev/sda1
