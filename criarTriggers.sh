#!/bin/bash
fonte="/home/cpd/arquivos"
copia="/home/cpd/arquivos/programas/.trigger "
echo "Copia iniciada em $(date +%d"/"%b"/"%Y" as "%T)"
########### copia dos arquivos para o backup #############
cp -f -R -u -v $copia$fonte/seguranca
cp -f -R -u -v $copia$fonte/adm
cp -f -R -u -v $copia$fonte/cimic
cp -f -R -u -v $copia$fonte/comunicados
cp -f -R -u -v $copia$fonte/cpd
cp -f -R -u -v $copia$fonte/cras
cp -f -R -u -v $copia$fonte/financas
cp -f -R -u -v $copia$fonte/infra
cp -f -R -u -v $copia$fonte/judiciaria
cp -f -R -u -v $copia$fonte/np
cp -f -R -u -v $copia$fonte/producao
cp -f -R -u -v $copia$fonte/saude
cp -f -R -u -v $copia$fonte/turno1
cp -f -R -u -v $copia$fonte/turno2
cp -f -R -u -v $copia$fonte/turno3
cp -f -R -u -v $copia$fonte/turno4
cp -f -R -u -v $copia$fonte/sindicancia
cp -f -R -u -v $copia$fonte/publico
cp -f -R -u -v $copia$fonte/rol
cp -f -R -u -v $copia$fonte/notes
cp -f -R -u -v $copia$fonte/peculio
#########################################################
clear
echo -e "Copia concluido $(date +%d"/"%b"/"%Y" as "%T)\n" 
